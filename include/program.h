#ifndef PROGRAM_H
#define PROGRAM_H

#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

/*
layout (location=0, rgba32f) uniform image2D image;
layout (location=1) uniform samplerCube skybox;
layout (location=2) uniform mat4 global_transform;
layout (location=3) uniform vec3 color_match;
layout (location=5) uniform float global_seed;
layout (location=4) uniform uint shapes_count;
layout (location=6) uniform uint wavelength;

layout (location=7) uniform uvec3 MacroGroupID;
layout (location=8) uniform uvec3 MacroGroupSize;
layout (location=9) uniform uvec3 NumMacroGroups;
*/

enum {
	IMAGE_LOCATION,
	SHAPES_COUNT_LOCATION,
	TRANSFORM_LOCATION,
	MACRO_GROUP_ID_LOCATION,
	NUM_MACRO_GROUPS_LOCATION,
	SEED_LOCATION,
	COLOR_MATCH_LOCATION,
	REFLECTIVE_STRENGTH_LOCATION,
	WAVELENGTH_LOCATION
} UniformLocations;

GLuint locations[64];

// returns an error code 
GLuint load_and_compile_shader(GLuint* shader, GLint type, char* filename);

GLuint link_program(GLuint program,
    GLuint vertex_shader, 
    GLuint tess_ctrl_shader, 
    GLuint tess_eval_shader, 
    GLuint geometry_shader, 
    GLuint fragment_shader, 
    GLuint compute_shader);

void build_graphics_program(GLuint* program);
void build_compute_program(GLuint* program, GLuint* locations);

#endif // !PROGRAM_H
