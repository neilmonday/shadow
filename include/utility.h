#ifndef UTILITY_H
#define UTILITY_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <windows.h>
#include <tchar.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

// Vectors
typedef struct
{
    GLfloat x, y;
} vec2;

typedef struct
{
    GLfloat x, y, z;
} vec3;

typedef struct
{
    GLfloat x, y, z, w;
} vec4;

// Double Vectors
typedef struct
{
    GLdouble x, y;
} dvec2;

typedef struct
{
    GLdouble x, y, z;
} dvec3;

typedef struct
{
    GLdouble x, y, z, w;
} dvec4;

// Matrices 
typedef struct
{
    vec2 x, y;
} mat2;

typedef struct
{
    vec3 x, y, z;
} mat3;

typedef struct
{
    vec4 x, y, z, w;
} mat4;

// Double Matrices
typedef struct
{
    dvec2 x, y;
} dmat2;

typedef struct
{
    dvec3 x, y, z;
} dmat3;

typedef struct
{
    dvec4 x, y, z, w;
} dmat4;

GLfloat length(vec3 vector);

GLfloat dot(vec3 A, vec3 B);

vec3 cross(vec3 A, vec3 B);

vec3 subtract(vec3 A, vec3 B);

vec3 normalize(vec3 vector);

mat4 look_at(vec3 position, vec3 target, vec3 up);

mat4 perspective(GLfloat fovy, GLfloat aspect, GLfloat zNear, GLfloat zFar);

vec4 multiply_mat4_vec4(mat4*const A, vec4*const B);
vec3 multiply_mat4_vec3(mat4*const A, vec3*const B);

mat4 multiply_mat4(mat4*const A, mat4*const B);

dmat4 multiply_dmat4(dmat4*const A, dmat4*const B);

mat4 identity();

mat4 zero();

//int rotate(mat4* rotation, GLfloat phi, GLfloat theta, GLfloat psi);

mat4 rotate(vec3 rotation);

mat4 translate(vec3 translation);

vec3 calculate_rotation(GLFWwindow* window, dvec2 initial_cursor);

//void calculate_translation(GLFWwindow* window, GLdouble* const phi, GLdouble* const theta, GLdouble* const psi,  GLdouble* x, GLdouble* y, GLdouble* z);

void calculate_translation(GLFWwindow* window, vec3* rotation, vec3* position);

HANDLE init_watch_folder();

void watch_folder(HANDLE dwChangeHandle, void(*rebuild)(GLuint*, GLuint*), GLuint* compute_program, GLuint* locations);

GLdouble normal_distribution(GLdouble x, GLdouble mu, GLdouble sigma);

void process_keypresses(GLFWwindow* window, GLuint* wireframe);

#endif //UTILITY_H
