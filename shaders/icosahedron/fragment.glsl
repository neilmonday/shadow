#version 450 core

in vec3 g_facet_normal;
in vec3 g_position;
in vec3 g_light_position;
in vec3 g_view_position;

void main()
{
    vec3 light_color = vec3(1.0);
    vec3 base_color = vec3(1.0);

    //helpful for a couple of the components
    vec3 normal = normalize(g_facet_normal);
    vec3 view_direction = normalize(g_view_position - g_position);
    vec3 light_direction = normalize(g_light_position - g_position);

    //ambient
    float ambient_strength = 0.1;
    float ambient = ambient_strength;

    //specular
    float specular_strength = 0.5;
    vec3 reflection_direction = reflect(-light_direction, normal); 
    float spec = pow(max(dot(view_direction, reflection_direction), 0.0), 32);
    vec3 specular = specular_strength * spec * light_color;  

    //diffuse
    float diffuse_strength = max(dot(normal, light_direction), 0.0);
    vec3 diffuse = diffuse_strength * light_color;

    gl_FragColor = vec4((ambient + diffuse + specular) * base_color, 1.0);
}