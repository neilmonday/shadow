#version 450 core

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec3 light_position;
uniform vec3 view_position;

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec3 te_position[3];
in vec3 te_patch_distance[3];
in vec3 te_color[3];
in int te_instance_id[3];

out vec3 g_facet_normal;
//out vec3 g_patch_distance;
//out vec3 g_tri_distance;
//out vec3 g_color;

out vec3 g_position;
out vec3 g_light_position;
out vec3 g_view_position;

void main()
{
    vec3 A = te_position[2] - te_position[0];
    vec3 B = te_position[1] - te_position[0];

    //g_patch_distance = te_patch_distance[0];
    //g_tri_distance = vec3(1, 0, 0);
    g_facet_normal = normalize(cross(A, B));
	g_light_position = vec4(model * vec4(light_position, 1.0)).xyz;
	g_view_position = vec4(model * vec4(view_position, 1.0)).xyz;
    g_position = (model * (gl_in[0].gl_Position)).xyz;
    gl_Position = projection * view * model * (gl_in[0].gl_Position);
    //g_color = te_color[0];
    EmitVertex();

    //g_patch_distance = te_patch_distance[1];
    //g_tri_distance = vec3(0, 1, 0);
    g_facet_normal = normalize(cross(A, B));
	g_light_position = vec4(model * vec4(light_position, 1.0)).xyz;
	g_view_position = vec4(model * vec4(view_position, 1.0)).xyz;
    g_position = (model * (gl_in[1].gl_Position)).xyz;
    gl_Position = projection * view * model * (gl_in[1].gl_Position);
    //g_color = te_color[1];
    EmitVertex();

    //g_patch_distance = te_patch_distance[2];
    //g_tri_distance = vec3(0, 0, 1);
    g_facet_normal = normalize(cross(A, B));
	g_light_position = vec4(model * vec4(light_position, 1.0)).xyz;
	g_view_position = vec4(model * vec4(view_position, 1.0)).xyz;
    g_position = (model * (gl_in[2].gl_Position)).xyz;
    gl_Position = projection * view * model * (gl_in[2].gl_Position);
    //g_color = te_color[2];
    EmitVertex();

    EndPrimitive();
}