#version 450 core

layout(vertices = 3) out;

uniform float tess_level_inner;
uniform float tess_level_outer;

in vec3 vs_position[];
in int vs_instance_id[];

out vec3 tc_position[];
out int tc_instance_id[];

void main()
{
    tc_position[gl_InvocationID] = vs_position[gl_InvocationID];
    tc_instance_id[gl_InvocationID] = vs_instance_id[gl_InvocationID];

    if (gl_InvocationID == 0) {
        gl_TessLevelInner[0] = tess_level_inner;
        gl_TessLevelOuter[0] = tess_level_outer;
        gl_TessLevelOuter[1] = tess_level_outer;
        gl_TessLevelOuter[2] = tess_level_outer;
    }
}