#version 450 core

layout(triangles, equal_spacing, cw) in;

in vec3 tc_position[];
in int tc_instance_id[];

out vec3 te_position;
out vec3 te_patch_distance;
out vec3 te_color;
out int te_instance_id;

void main()
{
    te_instance_id = tc_instance_id[0];
    vec3 p0 = gl_TessCoord.x * tc_position[0];
    vec3 p1 = gl_TessCoord.y * tc_position[1];
    vec3 p2 = gl_TessCoord.z * tc_position[2];
    te_patch_distance = gl_TessCoord;
    te_position = normalize(p0 + p1 + p2);
    te_color = (te_position + 1.0)/2.0;
    gl_Position = vec4(te_position, 1);
}