#version 450 core

layout(location=0) in vec3 position;

out vec3 vs_position;
out int vs_instance_id;

void main()
{
	vs_instance_id = gl_InstanceID;
	vs_position = position;
}