#version 450 core

layout(binding=0) uniform sampler2D shadow_map;

in vec3 vs_normal;
in vec3 vs_position;
in vec3 vs_light_position;
in vec3 vs_view_position;
in vec4 vs_position_light_space;

float shadow_calculation(vec4 position_light_space)
{
    // perform perspective divide
    vec3 projected_position = position_light_space.xyz / position_light_space.w;
    projected_position = projected_position * 0.5 + 0.5;

    float closest_depth = texture(shadow_map, projected_position.xy).r;
    float current_depth = projected_position.z;
    float bias = 0.02;
    float shadow = current_depth - bias > closest_depth  ? 1.0 : 0.0;
    return shadow;
}

void main()
{
    vec3 light_color = vec3(1.0);
    vec3 base_color = vec3(1.0);

    //helpful for a couple of the components
    vec3 normal = normalize(vs_normal);
    vec3 view_direction = normalize(vs_view_position - vs_position);
    vec3 light_direction = normalize(vs_light_position - vs_position);

    //ambient
    float ambient_strength = 0.1;
    float ambient = ambient_strength;

    //specular
    float specular_strength = 0.5;
    vec3 reflection_direction = reflect(-light_direction, normal); 
    float spec = pow(max(dot(view_direction, reflection_direction), 0.0), 32);
    vec3 specular = specular_strength * spec * light_color;  

    //diffuse
    float diffuse_strength = max(dot(normal, light_direction), 0.0);
    vec3 diffuse = diffuse_strength * light_color;

    //shadow
    float shadow = shadow_calculation(vs_position_light_space);       

    gl_FragColor = vec4((ambient + (1.0 - shadow) * (diffuse + specular)) * base_color, 1.0);
}