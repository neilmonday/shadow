#version 450 core

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 light_space;

uniform vec3 light_position;
uniform vec3 view_position;

layout(location=0) in vec3 position;
layout(location=1) in vec3 normal;

out vec3 vs_normal;
out vec3 vs_position;
out vec3 vs_light_position;
out vec3 vs_view_position;
out vec4 vs_position_light_space;

void main()
{
	vs_normal = vec4(model * vec4(normal, 1.0)).xyz;
	vs_position = vec4(model * vec4(position, 1.0)).xyz;
	vs_light_position = vec4(model * vec4(light_position, 1.0)).xyz;
	vs_view_position = vec4(model * vec4(view_position, 1.0)).xyz;
	vs_position_light_space = light_space * vec4(vs_position, 1.0);
	gl_Position = projection * view * model * vec4(position, 1.0);
}
