#include <stdio.h>
#include <assert.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "program.h"
#include "utility.h"

//view_position changes.
vec3 view_position = { 0.0f, 0.0f, -10.0f };
vec3 light_position = { 26.0, 21.0, 5.0 };
const vec3 up = { 0.0f, 1.0f, 0.0f };
const vec3 origin = { 0.0f, 0.0f, 0.0f };

//program IDs
GLuint icosahedron_program = 0;
GLuint plane_program = 0;

//GLFW state
dvec2 initial_cursor;

//transformation
GLuint icosahedron_model_handle;
GLuint icosahedron_view_handle;
GLuint icosahedron_projection_handle;
GLuint icosahedron_light_space_handle;

//tess ctrl
GLuint icosahedron_tess_level_inner_handle;
GLuint icosahedron_tess_level_outer_handle;

//fragment
GLuint icosahedron_light_position_handle;
GLuint icosahedron_view_position_handle;

//transformation
GLuint plane_model_handle;
GLuint plane_view_handle;
GLuint plane_projection_handle;
GLuint plane_light_space_handle;

//fragment
GLuint plane_light_position_handle;
GLuint plane_view_position_handle;

//GL stuff
GLuint vao[2];
GLuint vbo[2];
GLuint ibo[2];
GLuint vertices_count[2];
GLuint indices_count[2];

//shadow map
GLuint shadow_map;
GLuint shadow_width = 1024, shadow_height = 1024;
GLuint fbo[1];

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);

void window_size_callback(GLFWwindow* window, int width, int height);

void setup_icosahedron(GLuint* vao, GLuint* vbo, GLuint* ibo, GLuint* vertices_count, GLuint* indices_count);

void setup_plane(GLuint* vao, GLuint* vbo, GLuint* ibo, GLuint* vertices_count, GLuint* indices_count);

void render_scene(GLFWwindow* window);

void draw_scene(mat4 *model, mat4 *view, mat4 *projection);

void main(void)
{
    GLFWwindow* window;
    // Initialise GLFW
    if (!glfwInit())
        return;

    int window_type = 1;

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    if(window_type == 0)
    {
        GLFWmonitor* monitor = glfwGetPrimaryMonitor();
        const GLFWvidmode* mode = glfwGetVideoMode(monitor);
        glfwWindowHint(GLFW_RED_BITS, mode->redBits);
        glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
        glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
        glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate); 
        glfwWindowHint(GLFW_SAMPLES, 4);

        // Create a fullscreen mode window and its OpenGL context
        //window = glfwCreateWindow(mode->width, mode->height, "SHADOW", monitor, NULL);
        window = glfwCreateWindow(1920, 1080, "SHADOW", monitor, NULL);
    }
    else if (window_type == 1)
    {
        int initial_width = 1280;
        int initial_height = 720;

        // Create a windowed mode window and its OpenGL context 
        window = glfwCreateWindow(initial_width, initial_height, "SHADOW", NULL, NULL);
    }

    if (!window)
    {
        glfwTerminate();
        return;
    }
    
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glfwSetWindowSizeCallback(window, &window_size_callback);
    glfwSetKeyCallback(window, &key_callback);

    // Make the window's context current
    glfwMakeContextCurrent(window);

    // Initialize GLEW
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();

    if (GLEW_OK != err)
        return;

    GLint major = 0;
    GLint minor = 0;
    glGetIntegerv(GL_MAJOR_VERSION, &major);
    glGetIntegerv(GL_MINOR_VERSION, &minor);
    const GLubyte* version = glGetString(GL_VERSION);
    const GLubyte* renderer = glGetString(GL_RENDERER);

    // Initialize
    //setup shadow map
    glCreateFramebuffers(1, &fbo[0]);
    glActiveTexture(GL_TEXTURE0);
    glCreateTextures(GL_TEXTURE_2D, 1, &shadow_map);
    glTextureStorage2D(shadow_map, 1, GL_DEPTH_COMPONENT32, shadow_width * 2, shadow_height * 2);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glBindFramebuffer(GL_FRAMEBUFFER, fbo[0]);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, shadow_map, 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    //build shader programs
    build_icosahedron_program(&icosahedron_program);
    build_plane_program(&plane_program);

    setup_icosahedron(&vao[0], &vbo[0], &ibo[0], &vertices_count[0], &indices_count[0]);

    setup_plane(&vao[1], &vbo[1], &ibo[1], &vertices_count[1], &indices_count[1]);

    glUseProgram(icosahedron_program);
    // Get a handle to fragment shader's iGlobalTime uniform.

    //transformation
    icosahedron_model_handle = glGetUniformLocation(icosahedron_program, "model");
    icosahedron_view_handle = glGetUniformLocation(icosahedron_program, "view");
    icosahedron_projection_handle = glGetUniformLocation(icosahedron_program, "projection");

    //tess ctrl
    icosahedron_tess_level_inner_handle = glGetUniformLocation(icosahedron_program, "tess_level_inner");
    icosahedron_tess_level_outer_handle = glGetUniformLocation(icosahedron_program, "tess_level_outer");

    //fragment
    icosahedron_light_position_handle = glGetUniformLocation(plane_program, "light_position");
    icosahedron_view_position_handle = glGetUniformLocation(plane_program, "view_position");

    glUseProgram(plane_program);
    // Get a handle to fragment shader's iGlobalTime uniform.

    //transformation
    plane_model_handle = glGetUniformLocation(plane_program, "model");
    plane_view_handle = glGetUniformLocation(plane_program, "view");
    plane_projection_handle = glGetUniformLocation(plane_program, "projection");
    plane_light_space_handle = glGetUniformLocation(plane_program, "light_space");

    //fragment
    plane_light_position_handle = glGetUniformLocation(plane_program, "light_position");
    plane_view_position_handle = glGetUniformLocation(plane_program, "view_position");
    GLfloat time = 0.0f;

    //GLFW state tracking
    glfwGetCursorPos(window, &(initial_cursor.x), &(initial_cursor.y));

    //depth
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    GLuint tess_level_inner = 4;
    GLuint tess_level_outer = 4;

    glUseProgram(icosahedron_program);
    glUniform1f(icosahedron_tess_level_inner_handle, tess_level_inner);
    glUniform1f(icosahedron_tess_level_outer_handle, tess_level_outer);

    err = glGetError(); assert(!err);
    // Loop until the user closes the window 
    while (!glfwWindowShouldClose(window))
    {
        render_scene(window);
	}

    glDeleteVertexArrays(1, &vao[0]);
    glDeleteVertexArrays(1, &vao[1]);
    glfwTerminate();
    return;
}

void render_scene(GLFWwindow* window)
{
    int width = 0, height = 0;
    glfwGetWindowSize(window, &width, &height);

    //0.785398 radians == 45 degrees.
    //1.5708 radians == 90 degrees.
    mat4 projection = perspective(0.785398, (float)shadow_width / (float)shadow_height, 20.0f, 100.0f);
    mat4 view = look_at(light_position, origin, up);    //I just chose for the light to "look" at the origin.
    mat4 model = identity();

    mat4 light_model_view = multiply_mat4(&model, &view);
    mat4 light_space = multiply_mat4(&light_model_view, &projection);

    glClearColor(0.01, 0.01, 0.01, 1.0);
    glClearDepth(1.0);

    //draw scene to shadow map from light perspective
    glViewport(0, 0, shadow_width*2, shadow_height*2);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo[0]);
    glClear(GL_DEPTH_BUFFER_BIT);
    draw_scene(&model, &view, &projection, &light_space);

    vec3 forward = { 0.0f, 0.0f, 1.0f };
    vec3 rotation = calculate_rotation(window, initial_cursor);
    mat4 rotation_matrix = rotate(rotation);
    forward = multiply_mat4_vec3(&rotation_matrix, &forward);

    calculate_translation(window, &rotation, &view_position);

    //0.785398 radians == 45 degrees.
    //1.5708 radians == 90 degrees.
    projection = perspective(0.785398, (float)width / (float)height, 0.5f, 10000.0f);
    vec3 position_plus_forward = { view_position.x + forward.x, view_position.y + forward.y, view_position.z + forward.z };
    view = look_at(view_position, position_plus_forward, up);
    model = identity();

    //draw scene as normal
    glViewport(0, 0, width, height);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBindTexture(GL_TEXTURE_2D, shadow_map);
    draw_scene(&model, &view, &projection, &light_space);

    //Draw the image to the screen
    glfwSwapBuffers(window);
    glfwPollEvents();
}

void draw_scene(mat4 *model, mat4 *view, mat4 *projection, mat4* light_space)
{
    glUseProgram(icosahedron_program);
    glPatchParameteri(GL_PATCH_VERTICES, 3);       // tell OpenGL that every patch has 16 verts
    glUniformMatrix4fv(icosahedron_model_handle, 1, GL_FALSE, model);
    glUniformMatrix4fv(icosahedron_view_handle, 1, GL_FALSE, view);
    glUniformMatrix4fv(icosahedron_projection_handle, 1, GL_FALSE, projection);
    glUniform3f(plane_light_position_handle, light_position.x, light_position.y, light_position.z);
    glUniform3f(plane_view_position_handle, view_position.x, view_position.y, view_position.z);
    glBindVertexArray(vao[0]);
    glDrawElements(GL_PATCHES, indices_count[0], GL_UNSIGNED_INT, (void*)0);

    glUseProgram(plane_program);
    glUniformMatrix4fv(plane_model_handle, 1, GL_FALSE, model);
    glUniformMatrix4fv(plane_view_handle, 1, GL_FALSE, view);
    glUniformMatrix4fv(plane_projection_handle, 1, GL_FALSE, projection);
    glUniformMatrix4fv(plane_light_space_handle, 1, GL_FALSE, light_space);
    glUniform3f(plane_light_position_handle, light_position.x, light_position.y, light_position.z);
    glUniform3f(plane_view_position_handle, view_position.x, view_position.y, view_position.z);
    glBindVertexArray(vao[1]);
    glDrawElements(GL_TRIANGLES, indices_count[1], GL_UNSIGNED_INT, (void*)0);
}

void setup_icosahedron(GLuint* vao, GLuint* vbo, GLuint* ibo, GLuint* vertices_count, GLuint* indices_count)
{
    // create 12 vertices of a icosahedron
    GLfloat t = (1.0 + sqrt(5.0)) / 2.0;
    GLfloat vertices[] = {
        -1,  t,  0,
        1,  t,  0,
        -1, -t,  0,
        1, -t,  0,

        0, -1,  t,
        0,  1,  t,
        0, -1, -t,
        0,  1, -t,

        t,  0, -1,
        t,  0,  1,
        -t,  0, -1,
        -t,  0,  1,
    };

    GLuint indices[] = {
        // 5 faces around point 0
        0, 11, 5,
        0, 5, 1,
        0, 1, 7,
        0, 7, 10,
        0, 10, 11,

        // 5 adjacent faces
        1, 5, 9,
        5, 11, 4,
        11, 10, 2,
        10, 7, 6,
        7, 1, 8,

        // 5 faces around point 3
        3, 9, 4,
        3, 4, 2,
        3, 2, 6,
        3, 6, 8,
        3, 8, 9,

        // 5 adjacent faces
        4, 9, 5,
        2, 4, 11,
        6, 2, 10,
        8, 6, 7,
        9, 8, 1,
    };

    GLuint err = glGetError(); assert(!err);

    *vertices_count = sizeof(vertices) / (sizeof(vertices[0]) * 3);
    *indices_count = sizeof(indices) / sizeof(indices[0]);

    glCreateBuffers(1, vbo);
    glNamedBufferStorage(*vbo, *vertices_count * 3 * sizeof(GLfloat), vertices, 0);
    err = glGetError(); assert(!err);
    glCreateBuffers(1, ibo);
    glNamedBufferStorage(*ibo, *indices_count * sizeof(GLuint), indices, 0);
    err = glGetError(); assert(!err);

    glCreateVertexArrays(1, vao);
    err = glGetError(); assert(!err);

    glVertexArrayVertexBuffer(*vao, 0, *vbo, 0, 3 * sizeof(GLfloat));
    glVertexArrayElementBuffer(*vao, *ibo);
    err = glGetError(); assert(!err);

    glEnableVertexArrayAttrib(*vao, 0);
    //glEnableVertexArrayAttrib(vao, 1);
    //glEnableVertexArrayAttrib(vao, 2);
    err = glGetError(); assert(!err);

    glVertexArrayAttribFormat(*vao, 0, 3, GL_FLOAT, GL_FALSE, 0);
    //glVertexArrayAttribFormat(vao, 1, 3, GL_FLOAT, GL_FALSE, offsetof(vertex_t, nrm));
    //glVertexArrayAttribFormat(vao, 2, 2, GL_FLOAT, GL_FALSE, offsetof(vertex_t, tex));
    err = glGetError(); assert(!err);

    glVertexArrayAttribBinding(*vao, 0, 0);
    //glVertexArrayAttribBinding(vao, 1, 0);
    //glVertexArrayAttribBinding(vao, 2, 0);
    err = glGetError(); assert(!err);
}

void setup_plane(GLuint* vao, GLuint* vbo, GLuint* ibo, GLuint* vertices_count, GLuint* indices_count)
{
    GLfloat vertices[] = {
        -10.0, -2.0, -10.0, 0.0f, 1.0f, 0.0f,
         10.0, -2.0, -10.0, 0.0f, 1.0f, 0.0f,
        -10.0, -2.0,  10.0, 0.0f, 1.0f, 0.0f,
         10.0, -2.0,  10.0, 0.0f, 1.0f, 0.0f,
    };

    GLuint indices[] = {
        0, 1, 2,
        2, 1, 3
    };

    GLuint err = glGetError(); assert(!err);

    *vertices_count = sizeof(vertices) / (sizeof(vertices[0]) * 6);
    *indices_count = sizeof(indices) / sizeof(indices[0]);

    glCreateBuffers(1, vbo);
    glNamedBufferStorage(*vbo, *vertices_count * 6 * sizeof(GLfloat), vertices, 0);
    err = glGetError(); assert(!err);
    glCreateBuffers(1, ibo);
    glNamedBufferStorage(*ibo, *indices_count * sizeof(GLuint), indices, 0);
    err = glGetError(); assert(!err);

    glCreateVertexArrays(1, vao);
    err = glGetError(); assert(!err);

    glVertexArrayVertexBuffer(*vao, 0, *vbo, 0, 6 * sizeof(GLfloat));
    glVertexArrayElementBuffer(*vao, *ibo);
    err = glGetError(); assert(!err);

    glEnableVertexArrayAttrib(*vao, 0);
    glEnableVertexArrayAttrib(*vao, 1);
    //glEnableVertexArrayAttrib(vao, 2);
    err = glGetError(); assert(!err);

    glVertexArrayAttribFormat(*vao, 0, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribFormat(*vao, 1, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat));
    //glVertexArrayAttribFormat(vao, 2, 2, GL_FLOAT, GL_FALSE, offsetof(vertex_t, tex));
    err = glGetError(); assert(!err);

    glVertexArrayAttribBinding(*vao, 0, 0);
    glVertexArrayAttribBinding(*vao, 1, 0);
    //glVertexArrayAttribBinding(vao, 2, 0);
    err = glGetError(); assert(!err);
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    GLint polygon_mode[2];
    glGetIntegerv(GL_POLYGON_MODE, polygon_mode);

    if ((key == GLFW_KEY_P) && (action == GLFW_PRESS))
    {
        if (polygon_mode[0] == GL_FILL)
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        }
        else if (polygon_mode[0] == GL_LINE)
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        else
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
    }

    if (((key == GLFW_KEY_UP) || (key == GLFW_KEY_DOWN) || (key == GLFW_KEY_LEFT) || (key == GLFW_KEY_RIGHT)) && (action == GLFW_PRESS))
    {
        GLuint original_program;
        glGetIntegerv(GL_CURRENT_PROGRAM, &original_program);
        glUseProgram(icosahedron_program);
        GLuint tess_level_inner_handle = glGetUniformLocation(icosahedron_program, "tess_level_inner");
        GLuint tess_level_outer_handle = glGetUniformLocation(icosahedron_program, "tess_level_outer");

        GLfloat tess_level_inner;
        GLfloat tess_level_outer;
        glGetUniformfv(icosahedron_program, tess_level_inner_handle, &tess_level_inner);
        glGetUniformfv(icosahedron_program, tess_level_outer_handle, &tess_level_outer);

        if (key == GLFW_KEY_UP)
        {
            tess_level_inner += 1;
        }
        if (key == GLFW_KEY_DOWN)
        {
            tess_level_inner -= 1;
        }
        if (key == GLFW_KEY_RIGHT)
        {
            tess_level_outer += 1;
        }
        if (key == GLFW_KEY_LEFT)
        {
            tess_level_outer -= 1;
        }

        glUniform1f(tess_level_inner_handle, tess_level_inner);
        glUniform1f(tess_level_outer_handle, tess_level_outer);

        glUseProgram(original_program);
    }
}

void window_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}
