#include "utility.h"

const GLdouble PI = 3.1415926535897932384626433832795;
const GLdouble E = 2.7182818284590452353602874713526;

vec4 multiply_mat4_vec4(mat4*const A, vec4*const B)
{
    vec4 result;

    result.x = A->x.x * B->x + A->x.y * B->y + A->x.z * B->z + A->x.w * B->w;
    result.y = A->y.x * B->x + A->y.y * B->y + A->y.z * B->z + A->y.w * B->w;
    result.z = A->z.x * B->x + A->z.y * B->y + A->z.z * B->z + A->z.w * B->w;
    result.w = A->w.x * B->x + A->w.y * B->y + A->w.z * B->z + A->w.w * B->w;

    return result;
}

vec3 multiply_mat4_vec3(mat4*const A, vec3*const B)
{
    vec3 result;

    result.x = A->x.x * B->x + A->x.y * B->y + A->x.z * B->z;
    result.y = A->y.x * B->x + A->y.y * B->y + A->y.z * B->z;
    result.z = A->z.x * B->x + A->z.y * B->y + A->z.z * B->z;

    return result;
}

mat4 multiply_mat4(mat4*const A, mat4*const B)
{
    mat4 result;

    result.x.x = A->x.x * B->x.x + A->x.y * B->y.x + A->x.z * B->z.x + A->x.w * B->w.x;
    result.x.y = A->x.x * B->x.y + A->x.y * B->y.y + A->x.z * B->z.y + A->x.w * B->w.y;
    result.x.z = A->x.x * B->x.z + A->x.y * B->y.z + A->x.z * B->z.z + A->x.w * B->w.z;
    result.x.w = A->x.x * B->x.w + A->x.y * B->y.w + A->x.z * B->z.w + A->x.w * B->w.w;

    result.y.x = A->y.x * B->x.x + A->y.y * B->y.x + A->y.z * B->z.x + A->y.w * B->w.x;
    result.y.y = A->y.x * B->x.y + A->y.y * B->y.y + A->y.z * B->z.y + A->y.w * B->w.y;
    result.y.z = A->y.x * B->x.z + A->y.y * B->y.z + A->y.z * B->z.z + A->y.w * B->w.z;
    result.y.w = A->y.x * B->x.w + A->y.y * B->y.w + A->y.z * B->z.w + A->y.w * B->w.w;

    result.z.x = A->z.x * B->x.x + A->z.y * B->y.x + A->z.z * B->z.x + A->z.w * B->w.x;
    result.z.y = A->z.x * B->x.y + A->z.y * B->y.y + A->z.z * B->z.y + A->z.w * B->w.y;
    result.z.z = A->z.x * B->x.z + A->z.y * B->y.z + A->z.z * B->z.z + A->z.w * B->w.z;
    result.z.w = A->z.x * B->x.w + A->z.y * B->y.w + A->z.z * B->z.w + A->z.w * B->w.w;

    result.w.x = A->w.x * B->x.x + A->w.y * B->y.x + A->w.z * B->z.x + A->w.w * B->w.x;
    result.w.y = A->w.x * B->x.y + A->w.y * B->y.y + A->w.z * B->z.y + A->w.w * B->w.y;
    result.w.z = A->w.x * B->x.z + A->w.y * B->y.z + A->w.z * B->z.z + A->w.w * B->w.z;
    result.w.w = A->w.x * B->x.w + A->w.y * B->y.w + A->w.z * B->z.w + A->w.w * B->w.w;

    return result;
}

dmat4 multiply_dmat4(dmat4*const A, dmat4*const B)
{
    dmat4 result;

    result.x.x = A->x.x * B->x.x + A->x.y * B->y.x + A->x.z * B->z.x + A->x.w * B->w.x;
    result.x.y = A->x.x * B->x.y + A->x.y * B->y.y + A->x.z * B->z.y + A->x.w * B->w.y;
    result.x.z = A->x.x * B->x.z + A->x.y * B->y.z + A->x.z * B->z.z + A->x.w * B->w.z;
    result.x.w = A->x.x * B->x.w + A->x.y * B->y.w + A->x.z * B->z.w + A->x.w * B->w.w;

    result.y.x = A->y.x * B->x.x + A->y.y * B->y.x + A->y.z * B->z.x + A->y.w * B->w.x;
    result.y.y = A->y.x * B->x.y + A->y.y * B->y.y + A->y.z * B->z.y + A->y.w * B->w.y;
    result.y.z = A->y.x * B->x.z + A->y.y * B->y.z + A->y.z * B->z.z + A->y.w * B->w.z;
    result.y.w = A->y.x * B->x.w + A->y.y * B->y.w + A->y.z * B->z.w + A->y.w * B->w.w;

    result.z.x = A->z.x * B->x.x + A->z.y * B->y.x + A->z.z * B->z.x + A->z.w * B->w.x;
    result.z.y = A->z.x * B->x.y + A->z.y * B->y.y + A->z.z * B->z.y + A->z.w * B->w.y;
    result.z.z = A->z.x * B->x.z + A->z.y * B->y.z + A->z.z * B->z.z + A->z.w * B->w.z;
    result.z.w = A->z.x * B->x.w + A->z.y * B->y.w + A->z.z * B->z.w + A->z.w * B->w.w;

    result.w.x = A->w.x * B->x.x + A->w.y * B->y.x + A->w.z * B->z.x + A->w.w * B->w.x;
    result.w.y = A->w.x * B->x.y + A->w.y * B->y.y + A->w.z * B->z.y + A->w.w * B->w.y;
    result.w.z = A->w.x * B->x.z + A->w.y * B->y.z + A->w.z * B->z.z + A->w.w * B->w.z;
    result.w.w = A->w.x * B->x.w + A->w.y * B->y.w + A->w.z * B->z.w + A->w.w * B->w.w;

    return result;
}

mat4 identity()
{
    mat4 result;
    result.x.x = 1.0f;
    result.x.y = 0.0f;
    result.x.z = 0.0f;
    result.x.w = 0.0f;

    result.y.x = 0.0f;
    result.y.y = 1.0f;
    result.y.z = 0.0f;
    result.y.w = 0.0f;

    result.z.x = 0.0f;
    result.z.y = 0.0f;
    result.z.z = 1.0f;
    result.z.w = 0.0f;

    result.w.x = 0.0f;
    result.w.y = 0.0f;
    result.w.z = 0.0f;
    result.w.w = 1.0f;
    return result;
}

mat4 zero()
{
    mat4 result;
    result.x.x = 0.0f;
    result.x.y = 0.0f;
    result.x.z = 0.0f;
    result.x.w = 0.0f;

    result.y.x = 0.0f;
    result.y.y = 0.0f;
    result.y.z = 0.0f;
    result.y.w = 0.0f;

    result.z.x = 0.0f;
    result.z.y = 0.0f;
    result.z.z = 0.0f;
    result.z.w = 0.0f;

    result.w.x = 0.0f;
    result.w.y = 0.0f;
    result.w.z = 0.0f;
    result.w.w = 0.0f;
    return result;
}

mat4 rotate(vec3 rotation)
{
    //i should be able to combine these.
    mat4 rotation_phi = identity();

    rotation_phi.y.y = (GLfloat)cos(rotation.x);
    rotation_phi.y.z = (GLfloat)sin(rotation.x);

    rotation_phi.z.y = (GLfloat)-sin(rotation.x);
    rotation_phi.z.z = (GLfloat)cos(rotation.x);

    mat4 rotation_theta = identity();

    rotation_theta.x.x = (GLfloat)cos(rotation.y);
    rotation_theta.x.z = (GLfloat)-sin(rotation.y);

    rotation_theta.z.x = (GLfloat)sin(rotation.y);
    rotation_theta.z.z = (GLfloat)cos(rotation.y);

    mat4 result;
    result = multiply_mat4(&rotation_theta, &rotation_phi);
    return result;
}

mat4 translate(vec3 translation)
{
    mat4 result = identity();

    result.w.x = translation.x;
    result.w.y = translation.y;
    result.w.z = translation.z;

    return result;
}

vec3 calculate_rotation(GLFWwindow* window, dvec2 initial_cursor)
{
    dvec2 cursor = { 0.0, 0.0 };
    vec3 rotation = { 0.0f, 0.0f, 0.0f };

    glfwGetCursorPos(window, &(cursor.x), &(cursor.y));

    rotation.y = -(initial_cursor.x - cursor.x) / 300.0;
    rotation.x = (initial_cursor.y - cursor.y) / 300.0;

    if (rotation.x > 1.570)
        rotation.x = 1.570;
    if (rotation.x < -1.570)
        rotation.x = -1.570;
    /*printf("*****************\n");
    printf("Mouse Up:\n");
    printf("    Cursor : %f, %f\n", cursor.x, cursor.y);
    printf("  Rotation : %f, %f\n", rotation.x, rotation.y);
    printf("*****************\n");*/

    return rotation;
}

void calculate_translation(GLFWwindow* window, const vec3 rotation, vec3* position)
{
    GLdouble sensitivity = 0.002;
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        position->x -= sensitivity * sin(rotation.y)  *  cos(rotation.x);
        position->y -= sensitivity * 1                * -sin(rotation.x);
        position->z -= sensitivity * -cos(rotation.y) *  cos(rotation.x);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        position->x += sensitivity * cos(rotation.y);
        //*y -=;
        position->z += sensitivity * sin(rotation.y);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        position->x += sensitivity * sin(rotation.y)  *  cos(rotation.x);
        position->y += sensitivity * 1                * -sin(rotation.x);
        position->z += sensitivity * -cos(rotation.y) *  cos(rotation.x);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        position->x -= sensitivity * cos(rotation.y);
        //*y += ;
        position->z -= sensitivity * sin(rotation.y);
    }
}

GLfloat length(vec3 vector)
{
    return sqrt((vector.x * vector.x) + (vector.y * vector.y) + (vector.z * vector.z));
}

GLfloat dot(vec3 A, vec3 B)
{
    GLfloat result;
    result = A.x*B.x + A.y*B.y + A.z*B.z;
    return result;
}

vec3 cross(vec3 A, vec3 B)
{
    vec3 result;
    result.x = A.y*B.z - A.z*B.y;
    result.y = A.z*B.x - A.x*B.z;
    result.z = A.x*B.y - A.y*B.x;
    return result;
}

vec3 subtract(vec3 A, vec3 B)
{
    vec3 result;
    result.x = A.x - B.x;
    result.y = A.y - B.y;
    result.z = A.z - B.z;
    return result;
}

vec3 normalize(vec3 vector)
{
    vec3 result;
    GLfloat l = length(vector);
    result.x = vector.x / l;
    result.y = vector.y / l;
    result.z = vector.z / l;
    return result;
}

mat4 look_at(vec3 position, vec3 target, vec3 up)
{
    //printf("LookAt Pos: %f, %f, %f\n", position.x, position.y, position.z);
    //printf("LookAt Tar: %f, %f, %f\n", target.x, target.y, target.z);
    vec3  f = normalize(subtract(target, position));
    vec3  u = normalize(up);
    vec3  s = normalize(cross(f, u));
    u = cross(s, f);

    mat4 result = identity();
    result.x.x = s.x;
    result.y.x = s.y;
    result.z.x = s.z;
    result.w.x = -dot(s, position);
    result.x.y = u.x;
    result.y.y = u.y;
    result.z.y = u.z;
    result.w.y = -dot(u, position);
    result.x.z = -f.x;
    result.y.z = -f.y;
    result.z.z = -f.z;
    result.w.z = dot(f, position);
    return result;
}

mat4 perspective(GLfloat fovy, GLfloat aspect, GLfloat zNear, GLfloat zFar)
{
    GLfloat const tanHalfFovy = tan(fovy / 2.0f);

    mat4 result = zero();
    result.x.x = 1.0f / (aspect * tanHalfFovy);
    result.y.y = 1.0f / (tanHalfFovy);
    result.z.w = -1.0f;
    result.z.z = zFar / (zNear - zFar);
    result.w.z = -(zFar * zNear) / (zFar - zNear);

    return result;
}

HANDLE init_watch_folder( LPTSTR lpDir)
{
    // Wait for notification.
    HANDLE dwChangeHandle;
    TCHAR lpDrive[4];
    TCHAR lpFile[_MAX_FNAME];
    TCHAR lpExt[_MAX_EXT];

    _tsplitpath_s(lpDir, lpDrive, 4, NULL, 0, lpFile, _MAX_FNAME, lpExt, _MAX_EXT);

    lpDrive[2] = (TCHAR)'\\';
    lpDrive[3] = (TCHAR)'\0';

    // Watch the directory for file creation and deletion. 

    dwChangeHandle = FindFirstChangeNotification(
        lpDir,                         // directory to watch 
        TRUE,                         // do not watch subtree 
        FILE_NOTIFY_CHANGE_LAST_WRITE); // watch file name changes 

    if (dwChangeHandle == INVALID_HANDLE_VALUE)
    {
        printf("\n ERROR: FindFirstChangeNotification function failed.\n");
        ExitProcess(GetLastError());
    }

    // Make a final validation check on our handles.

    if (dwChangeHandle == NULL)
    {
        printf("\n ERROR: Unexpected NULL from FindFirstChangeNotification.\n");
        ExitProcess(GetLastError());
    }

    return dwChangeHandle;
}

void watch_folder(HANDLE dwChangeHandle, void(*rebuild)(GLuint*, GLuint*), GLuint* compute_program, GLuint* locations)
{
    DWORD dwWaitStatus = WaitForMultipleObjects(1, &dwChangeHandle, FALSE, 1);
    switch (dwWaitStatus)
    {
    case WAIT_OBJECT_0:
        printf("file has changed.\n");
        rebuild(compute_program, locations);
        if (FindNextChangeNotification(dwChangeHandle) == FALSE)
        {
            printf("\n ERROR: FindNextChangeNotification function failed.\n");
            ExitProcess(GetLastError());
        }
        break;
    case WAIT_TIMEOUT:
        //printf("No changes in the timeout period.\n");
        break;

    default:
        printf("ERROR: Unhandled dwWaitStatus.\n");
        ExitProcess(GetLastError());
        break;
    }
}

GLdouble normal_distribution(GLdouble x, GLdouble mu, GLdouble sigma)
{
    GLdouble coeffitient = 1 / sqrt(2 * PI*sigma*sigma);
    GLdouble exponent = -((x - mu) * (x - mu)) / (2 * sigma*sigma);
    return coeffitient * pow(E, exponent);
}